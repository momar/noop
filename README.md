# noop
Does nothing, forever.

Great to trick label-based services like [Traefik](https://doc.traefik.io/traefik/), or to disable services temporarily in `docker-compose.yml` files. Now with an always-healthy health check.

```bash
docker run -d momar/noop
```
