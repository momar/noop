FROM alpine AS build
RUN apk add --no-cache gcc musl-dev
COPY main.c /build/main.c
WORKDIR /build
RUN gcc -o noop -s -static main.c

FROM scratch
COPY --from=build /build/noop /noop
HEALTHCHECK --interval=1s --timeout=1s --retries=1 CMD ["/noop", "--healthcheck"]
ENTRYPOINT ["/noop"]
