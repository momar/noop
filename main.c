#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
void quit(int n) {
	exit(0);
}
int main(int argc, char *argv[]) {
	if (argc == 2 && strcmp(argv[1], "--healthcheck") == 0) {
		return(0);
	}
	signal(SIGINT, quit);
	signal(SIGTERM, quit);
	pause();
	return(0);
}
